package pe.com.core.test.unit.test;

import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.mockito.Matchers;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import pe.com.core.dao.ProductoDao;
import pe.com.core.entity.Producto;

@RunWith(MockitoJUnitRunner.class)
public class ProductoTest {

	@Mock
	private ProductoDao productoDao;
	@Mock
	private Producto producto;
	@Test
	public void CP01() {
        try{
            producto=new Producto();
            producto.setIdProducto(1);
            producto.setNombre("Nestle");
            producto.setIdCategoria(2);
            producto.setPrecio(12.0);
            when(productoDao.insertar(Matchers.any())).thenReturn(producto);
            productoDao.insertar(producto);
            Assert.assertTrue(producto.getIdProducto() > 0);
        }catch(Exception e){
            e.printStackTrace();
            Assert.fail();
        }
	}
	@Test
	public void CP02() {
		  try{
            producto=new Producto();
            producto.setIdProducto(1);
            producto.setNombre("Nestle");
            producto.setIdCategoria(2);
            producto.setPrecio(null);
            when(productoDao.insertar(Matchers.any())).thenReturn(producto);
            productoDao.insertar(producto).getIdProducto();
            Assert.assertTrue(producto.getIdProducto() > 0);
        }catch(Exception e){
            e.printStackTrace();
            Assert.fail();
        }
	}
	@Test
	public void CP03() {
		  try{
            producto=new Producto();
            producto.setIdProducto(1);
            producto.setNombre(null);
            producto.setIdCategoria(2);
            producto.setPrecio(12.0);
            when(productoDao.insertar(Matchers.any())).thenReturn(producto);
            productoDao.insertar(producto);
            Assert.assertTrue(producto.getIdProducto() > 0);
        }catch(Exception e){
            e.printStackTrace();
            Assert.fail();
        }
	}
	@Test
	public void CP04() {
		  try{
            producto=new Producto();
            producto.setIdProducto(1);
            producto.setNombre(null);
            producto.setIdCategoria(2);
            producto.setPrecio(null);
            when(productoDao.insertar(Matchers.any())).thenReturn(producto);
            productoDao.insertar(producto);
            Assert.assertTrue(producto.getIdProducto() > 0);
        }catch(Exception e){
            e.printStackTrace();
            Assert.fail();
        }
	}	
}
